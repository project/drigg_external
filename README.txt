ABOUT
-----
This modules provides a single javascript button to include in a page that will display either a
vote button or an "add" button. This module builds upon the Drigg set of modules.
It is meant to work as an external vote button for scoops availble inside a Drigg system.

The script to call to display the button is optimized not to hit the server too hard when a button is 
displayed multiple times on a site: each call to get the button is delayed by 1 second. So if you
have 10 buttons to display, the last one will be displayed after 10 seconds.

INSTALLATION
------------
Unzip in sites/all/modules as usual & enable the module

Extra pre-release steps:
 - go to js/button.js and change the base_url string. Don't forget the trailing slash
I will have this file generated from the admin page in the final release.

INSTALLING THE VOTE BUTTON ON example.com
-----------------------------------------
To display the button, simply add the following to the pages of the sites/blogs/etc you want the vote button 
to appear on:
	<script type="text/javascript">
	url_site = 'http://www.example.com/THE_URL_OF_THE_PAGE_ON_THE_SITE';
	</script>
	<script src="http://YOUR_DRIGG_SITE/sites/all/modules/drigg_external/js/button.js" type="text/javascript"></script>

The url_script is mandatory if you display the vote button on pages that can display multiple nodes (archives, etc),
it's the only way the server can know which URL to serve. If you are displaying the vote button only on pages
whose URL is the one in the DRIGG SITE then you can omit it.

FEEDBURNER ISSUE : feedburner changes the URL of the pages, you need to hack the aggregator module to get
the correct URLs in your drupal system. See http://www.drigg-code.org/node/158#comment-1434 and 
http://www.drigg-code.org/node/158#comment-1513

THEMING
-------
In both cases, output is done via a template which means that you can override the one
provided in the module which works well for extra_voting_forms.

The current implementation simply displays
the button but your implementation could:
 - add images
 - add backgrounds
 - add text & links
 - etc
 
You can do it all by writing your own themes.
 
 HOW TO DO IT:
  - copy the files files named *.tpl.php into your theme directory
  - change them to whatever you need.
 
 The magic variables are
  - for the vote button : $widget_html which contains the themed vote button according to drigg_ui configuration
  - for the add button : $link which contains the link with the 'Add this' text and to the "add drigg node" page. The
  form should have its URL filled out already. It does not fill out the title because these buttons can be
  displayed multiple times on the same page.
  
 All texts are processed through the t() function so you can localize them and change them to whatever you want
 in english.
 
 Cheers
 Philippe