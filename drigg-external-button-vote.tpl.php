<?php
// $Id

/**
 * @file
 * Default theme implementation for displaying the vote button on an external site
 *
 * Available variables:
 *  - $node: A drigg node (so node id is ->dnid)
 *  - $widget_html : the HTML vote the vote button, ready to be displayed
 *
 * @ingroup drigg_external
 */

//set header or else IE won't save the user's vote or find his session
drupal_set_header('P3P: CP="ALL ADM DEV PSAi COM OUR OTRo STP IND ONL"');
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

  <head>
    <?php
    //this could be optimized as we will be loading all the CSS even those we don't need
    //for the vote button. But it allows this module to work with any vote button with no
    //extra config. Plus, these CSS files are cached so it only hurts the first call
    //Plus, Drupal can be configured to collapse all of them into one so it doesn't really
    //matter
    print drupal_get_css();
    print drupal_get_js();
        ?>
  </head>
	<body style='background-color:transparent;' title='titre'  onload="parent.DRIGG_EVB_process_buttons()">
		<div class="drigg_external_button" id="drigg_external_button_<?php print $node->dnid; ?>">
			<center title="<?php print t('Support this author by voting !');?>"><?php print $widget_html; ?></center>
		</div>
	</body>
	</html>